
const LaunchRequestHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'LaunchRequest';
    },
    handle(handlerInput) {
        //_initializeApp(handlerInput);
        if(supportsAPL(handlerInput)) {
            handlerInput.responseBuilder.addDirective({
                type: 'Alexa.Presentation.APL.RenderDocument',
                version: '1.0',
                document: require('./templates/main.json'),
                datasources: require('./data/main.json')
            })
        }
        sessionAttributes.QUESTION_PROGRESS = -1;
        sessionAttributes.STATE = states.INITIALMODE;
        const CURRENT_TIMESTAMP = Math.floor(Date.now() / 1000);
        const repromptSpeechText = MISUNDERSTOOD_INSTRUCTIONS_ANSWER;
        var speechText = SHORT_WELCOME_MESSAGE;
        if(sessionAttributes.TIMESTAMP_WELCOME_MESSAGE !== 0 && (CURRENT_TIMESTAMP - sessionAttributes.TIMESTAMP_WELCOME_MESSAGE) <= WAIT_TIME_TO_SHORT_ANSWER) {
        speechText = SHORT_WELCOME_MESSAGE;
        } else {
        speechText = WELCOME_MESSAGE;
        }
        sessionAttributes.TIMESTAMP_WELCOME_MESSAGE = Math.floor(Date.now() / 1000);
        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt(repromptSpeechText)
            .getResponse();
    }
};
const YesIntentHandler = {
    canHandle( handlerInput ) {
        return handlerInput.requestEnvelope.request.type       	=== 'IntentRequest' &&
                handlerInput.requestEnvelope.request.intent.name === 'AMAZON.YesIntent';
    },
    handle( handlerInput ) {
        if(sessionAttributes.STATE === states.INITIALMODE) {
            sessionAttributes.STATE = states.QUIZMODE;
        } else if(sessionAttributes.STATE === states.QUIZMODE) {
            _recordanswer(handlerInput, 1);
        } else if(sessionAttributes.STATE === states.RESULTMODE) {
            sessionAttributes.QUESTION_PROGRESS = -1;
            sessionAttributes.STATE = states.QUIZMODE;
        }
        _nextQuestionOrResult(handlerInput);
        return handlerInput.responseBuilder
            .getResponse();
    }
};
const NoIntentHandler = {
    canHandle( handlerInput ) {
        return handlerInput.requestEnvelope.request.type       	=== 'IntentRequest' &&
                handlerInput.requestEnvelope.request.intent.name === 'AMAZON.NoIntent';
    },
    handle( handlerInput ) {
        if(sessionAttributes.STATE === states.INITIALMODE) {
            return CancelAndStopIntentHandler.handle(handlerInput);
        } else if(sessionAttributes.STATE === states.QUIZMODE) {
            _recordanswer(handlerInput, 0);
        } else if(sessionAttributes.STATE === states.RESULTMODE) {
            return handlerInput.responseBuilder
                .speak(GOODBYE_MESSAGE)
                .withShouldEndSession(true)
                .getResponse();
        }
        _nextQuestionOrResult(handlerInput);
        return handlerInput.responseBuilder
            .getResponse();
    }
};
const ResultIntentHandler = {
    canHandle( handlerInput ) {
        return handlerInput.requestEnvelope.request.type       	=== 'IntentRequest' &&
                handlerInput.requestEnvelope.request.intent.name === 'AMAZON.ResultIntent';
    },
    handle( handlerInput ) {
        const ans = ANSWERS.join("");
        const result = COMBINATIONS[ans][Math.floor(Math.random()*COMBINATIONS[ans].length)];
        const resultMessage = `${prependMessage} ${RESULT_MESSAGE} ${animalList[result].name} ${animalList[result].audio_message} ${PLAY_AGAIN_REQUEST}`;
        FINAL_MESSAGE.speechOutput = resultMessage;
        FINAL_MESSAGE.repromptSpeech = PLAY_AGAIN_REQUEST;
        FINAL_MESSAGE.cardTitle = animalList[result].display_name;
        FINAL_MESSAGE.cardContent = animalList[result].description;
        FINAL_MESSAGE.imageObj = animalList[result].img;
        
        if(supportsAPL(handlerInput)) {
            handlerInput.responseBuilder.addDirective({
                type: 'Alexa.Presentation.APL.RenderDocument',
                version: '1.0',
                document: require('./templates/main.json'),
                //datasources: require('./data/main.json')
                datasources: {
                    "bodyTemplate6Data": {
                        "type": "object",
                        "objectId": "bt6Sample",
                        "backgroundImage": {
                            "contentDescription": null,
                            "smallSourceUrl": null,
                            "largeSourceUrl": null,
                            "sources": [
                                {
                                    "url": FINAL_MESSAGE.imageObj.smallImageUrl,
                                    "size": "small",
                                    "widthPixels": 0,
                                    "heightPixels": 0
                                },
                                {
                                    "url": FINAL_MESSAGE.imageObj.largeImageUrl,
                                    "size": "large",
                                    "widthPixels": 0,
                                    "heightPixels": 0
                                }
                            ]
                        },
                        "textContent": {
                            "primaryText": {
                                "type": "PlainText",
                                "text": FINAL_MESSAGE.cardTitle
                            }
                        },
                        "logoUrl": "",
                        "hintText": ""
                    }
                }
            })
        }
        
        return handlerInput.responseBuilder
            .speak(FINAL_MESSAGE.speechOutput)
            .reprompt(FINAL_MESSAGE.repromptSpeech)
            .getResponse();
    }
};
const NextQuestionIntentHandler = {
    canHandle( handlerInput ) {
        return handlerInput.requestEnvelope.request.type       	=== 'IntentRequest' &&
                handlerInput.requestEnvelope.request.intent.name === 'AMAZON.NextQuestionIntent';
    },
    handle( handlerInput ) {
        sessionAttributes.QUESTION_PROGRESS++;
        // Reference current question to read:
        var currentQuestion = questions[sessionAttributes.QUESTION_PROGRESS].question;

        return handlerInput.responseBuilder
            .speak(`${prependMessage} ${_randomQuestionIntro()} ${currentQuestion}`)
            .reprompt(HELP_MESSAGE_AFTER_START)
            .withSimpleCard(currentQuestion)
            .getResponse();
    }
};
const RepeatIntentHandler = {
    canHandle( handlerInput ) {
        return handlerInput.requestEnvelope.request.type       	=== 'IntentRequest' &&
                handlerInput.requestEnvelope.request.intent.name === 'AMAZON.RepeatIntent';
    },
    handle( handlerInput ) {
        var MESSAGE = '';
        var REPROMP_MESSAGE = '';
        if(sessionAttributes.STATE === states.INITIALMODE) {
            MESSAGE = REPEATED_WELCOME_MESSAGE;
            REPROMP_MESSAGE = MISUNDERSTOOD_INSTRUCTIONS_ANSWER;
        } else if(sessionAttributes.STATE === states.QUIZMODE) {
            MESSAGE = questions[sessionAttributes.QUESTION_PROGRESS].question;
            REPROMP_MESSAGE = HELP_MESSAGE_AFTER_START;
        } else if(sessionAttributes.STATE === states.RESULTMODE) {
            MESSAGE = FINAL_MESSAGE.speechOutput;
            REPROMP_MESSAGE = FINAL_MESSAGE.repromptSpeech;
        }
        return handlerInput.responseBuilder
            .speak(MESSAGE)
            .reprompt(REPROMP_MESSAGE)
            .getResponse();
    }
};
const FallbackIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && handlerInput.requestEnvelope.request.intent.name === 'AMAZON.FallbackIntent';
    },
    handle(handlerInput) {
        return handlerInput.responseBuilder
            .speak(MISUNDERSTOOD_INSTRUCTIONS_ANSWER)
            .getResponse();
    }
}
const HelpIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && handlerInput.requestEnvelope.request.intent.name === 'AMAZON.HelpIntent';
    },
    handle(handlerInput) {
        return handlerInput.responseBuilder
            .speak(HELP_MESSAGE_AFTER_START)
            .reprompt(HELP_MESSAGE_AFTER_START)
            .getResponse();
    }
};
const CancelAndStopIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && (handlerInput.requestEnvelope.request.intent.name === 'AMAZON.CancelIntent'
                || handlerInput.requestEnvelope.request.intent.name === 'AMAZON.StopIntent');
    },
    handle(handlerInput) {
        return handlerInput.responseBuilder
            .speak(CANCEL_MESSAGE)
            .withShouldEndSession(true)
            .getResponse();
    }
};
const SessionEndedRequestHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'SessionEndedRequest';
    },
    handle(handlerInput) {
        return handlerInput.responseBuilder
            .speak(GOODBYE_MESSAGE)
            .getResponse();
        // Any cleanup logic goes here.
        //return handlerInput.responseBuilder.getResponse();
    }
};

// The intent reflector is used for interaction model testing and debugging.
// It will simply repeat the intent the user said. You can create custom handlers
// for your intents by defining them above, then also adding them to the request
// handler chain below.
const IntentReflectorHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest';
    },
    handle(handlerInput) {
        const intentName = handlerInput.requestEnvelope.request.intent.name;
        const speechText = `You just triggered ${intentName}`;

        return handlerInput.responseBuilder
            .speak(speechText)
            //.reprompt('add a reprompt if you want to keep the session open for the user to respond')
            .getResponse();
    }
};

// Generic error handling to capture any syntax or routing errors. If you receive an error
// stating the request handler chain is not found, you have not implemented a handler for
// the intent being invoked or included it in the skill builder below.
const ErrorHandler = {
    canHandle() {
        return true;
    },
    handle(handlerInput, error) {
        console.log(`~~~~ Error handled: ${error.message}`);
        const speechText = `Sorry, I couldn't understand what you said. Please try again.`;

        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt(speechText)
            .getResponse();
    }
};