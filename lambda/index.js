// This sample demonstrates handling intents from an Alexa skill using the Alexa Skills Kit SDK (v2).
// Please visit https://alexa.design/cookbook for additional examples on implementing slots, dialog management,
// session persistence, api calls, and more.
const Alexa = require('ask-sdk-core');

// Constants that alexa actually says
const Texts = require('texts.js');

// Private methods (this is the actual code logic behind the app)
const Functions = require('functions.js');

const LaunchRequestHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'LaunchRequest';
    },
    handle(handlerInput) {
        //_initializeApp(handlerInput);
        if(Functions.supportsAPL(handlerInput)) {
            handlerInput.responseBuilder.addDirective({
                type: 'Alexa.Presentation.APL.RenderDocument',
                version: '1.0',
                document: require('./templates/main.json'),
                datasources: require('./data/main.json')
            })
        }
        Functions.sessionAttributes.QUESTION_PROGRESS = -1;
        Functions.sessionAttributes.STATE = Functions.states.INITIALMODE;
        const CURRENT_TIMESTAMP = Math.floor(Date.now() / 1000);
        const repromptSpeechText = Texts.MISUNDERSTOOD_INSTRUCTIONS_ANSWER;
        var speechText = Texts.SHORT_WELCOME_MESSAGE;
        if(Functions.sessionAttributes.TIMESTAMP_WELCOME_MESSAGE !== 0 && (CURRENT_TIMESTAMP - Functions.sessionAttributes.TIMESTAMP_WELCOME_MESSAGE) <= Texts.WAIT_TIME_TO_SHORT_ANSWER) {
            speechText = Texts.SHORT_WELCOME_MESSAGE;
        } else {
            speechText = Texts.WELCOME_MESSAGE;
        }
        Functions.sessionAttributes.TIMESTAMP_WELCOME_MESSAGE = Math.floor(Date.now() / 1000);
        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt(repromptSpeechText)
            .getResponse();
    }
};
const YesIntentHandler = {
    canHandle( handlerInput ) {
        return handlerInput.requestEnvelope.request.type       	=== 'IntentRequest' &&
                handlerInput.requestEnvelope.request.intent.name === 'AMAZON.YesIntent';
    },
    handle( handlerInput ) {
        if(Functions.sessionAttributes.STATE === Functions.states.INITIALMODE) {
            Functions.sessionAttributes.STATE = Functions.states.QUIZMODE;
        } else if(Functions.sessionAttributes.STATE === Functions.states.QUIZMODE) {
            Functions._recordanswer(handlerInput, 1);
        } else if(Functions.sessionAttributes.STATE === Functions.states.RESULTMODE) {
            Functions.sessionAttributes.QUESTION_PROGRESS = -1;
            Functions.sessionAttributes.STATE = Functions.states.QUIZMODE;
        }
        Functions._nextQuestionOrResult(handlerInput);
        return handlerInput.responseBuilder
            .getResponse();
    }
};
const NoIntentHandler = {
    canHandle( handlerInput ) {
        return handlerInput.requestEnvelope.request.type       	=== 'IntentRequest' &&
                handlerInput.requestEnvelope.request.intent.name === 'AMAZON.NoIntent';
    },
    handle( handlerInput ) {
        if(Functions.sessionAttributes.STATE === Functions.states.INITIALMODE) {
            return CancelAndStopIntentHandler.handle(handlerInput);
        } else if(Functions.sessionAttributes.STATE === Functions.states.QUIZMODE) {
            Functions._recordanswer(handlerInput, 0);
        } else if(Functions.sessionAttributes.STATE === Functions.states.RESULTMODE) {
            return handlerInput.responseBuilder
                .speak(Texts.GOODBYE_MESSAGE)
                .withShouldEndSession(true)
                .getResponse();
        }
        Functions._nextQuestionOrResult(handlerInput);
        return handlerInput.responseBuilder
            .getResponse();
    }
};
const ResultIntentHandler = {
    canHandle( handlerInput ) {
        return handlerInput.requestEnvelope.request.type       	=== 'IntentRequest' &&
                handlerInput.requestEnvelope.request.intent.name === 'AMAZON.ResultIntent';
    },
    handle( handlerInput ) {
        const ans = Texts.ANSWERS.join("");
        const result = Texts.COMBINATIONS[ans][Math.floor(Math.random()*Texts.COMBINATIONS[ans].length)];
        const resultMessage = `${prependMessage} ${Texts.RESULT_MESSAGE} ${Texts.animalList[result].name} ${Texts.animalList[result].audio_message} ${Texts.PLAY_AGAIN_REQUEST}`;
        Texts.FINAL_MESSAGE.speechOutput = resultMessage;
        Texts.FINAL_MESSAGE.repromptSpeech = Texts.PLAY_AGAIN_REQUEST;
        Texts.FINAL_MESSAGE.cardTitle = Texts.animalList[result].display_name;
        Texts.FINAL_MESSAGE.cardContent = Texts.animalList[result].description;
        Texts.FINAL_MESSAGE.imageObj = Texts.animalList[result].img;
        
        if(Functions.supportsAPL(handlerInput)) {
            handlerInput.responseBuilder.addDirective({
                type: 'Alexa.Presentation.APL.RenderDocument',
                version: '1.0',
                document: require('./templates/main.json'),
                //datasources: require('./data/main.json')
                datasources: {
                    "bodyTemplate6Data": {
                        "type": "object",
                        "objectId": "bt6Sample",
                        "backgroundImage": {
                            "contentDescription": null,
                            "smallSourceUrl": null,
                            "largeSourceUrl": null,
                            "sources": [
                                {
                                    "url": Texts.FINAL_MESSAGE.imageObj.smallImageUrl,
                                    "size": "small",
                                    "widthPixels": 0,
                                    "heightPixels": 0
                                },
                                {
                                    "url": Texts.FINAL_MESSAGE.imageObj.largeImageUrl,
                                    "size": "large",
                                    "widthPixels": 0,
                                    "heightPixels": 0
                                }
                            ]
                        },
                        "textContent": {
                            "primaryText": {
                                "type": "PlainText",
                                "text": Texts.FINAL_MESSAGE.cardTitle
                            }
                        },
                        "logoUrl": "",
                        "hintText": ""
                    }
                }
            })
        }
        
        return handlerInput.responseBuilder
            .speak(Texts.FINAL_MESSAGE.speechOutput)
            .reprompt(Texts.FINAL_MESSAGE.repromptSpeech)
            .getResponse();
    }
};
const NextQuestionIntentHandler = {
    canHandle( handlerInput ) {
        return handlerInput.requestEnvelope.request.type       	=== 'IntentRequest' &&
                handlerInput.requestEnvelope.request.intent.name === 'AMAZON.NextQuestionIntent';
    },
    handle( handlerInput ) {
        Functions.sessionAttributes.QUESTION_PROGRESS++;
        // Reference current question to read:
        var currentQuestion = Texts.questions[Functions.sessionAttributes.QUESTION_PROGRESS].question;

        return handlerInput.responseBuilder
            .speak(`${prependMessage} ${Functions._randomQuestionIntro()} ${currentQuestion}`)
            .reprompt(Texts.HELP_MESSAGE_AFTER_START)
            .withSimpleCard(currentQuestion)
            .getResponse();
    }
};
const RepeatIntentHandler = {
    canHandle( handlerInput ) {
        return handlerInput.requestEnvelope.request.type       	=== 'IntentRequest' &&
                handlerInput.requestEnvelope.request.intent.name === 'AMAZON.RepeatIntent';
    },
    handle( handlerInput ) {
        var MESSAGE = '';
        var REPROMP_MESSAGE = '';
        if(Functions.sessionAttributes.STATE === Functions.states.INITIALMODE) {
            MESSAGE = Texts.REPEATED_WELCOME_MESSAGE;
            REPROMP_MESSAGE = Texts.MISUNDERSTOOD_INSTRUCTIONS_ANSWER;
        } else if(Functions.sessionAttributes.STATE === Functions.states.QUIZMODE) {
            MESSAGE = Texts.questions[Functions.sessionAttributes.QUESTION_PROGRESS].question;
            REPROMP_MESSAGE = Texts.HELP_MESSAGE_AFTER_START;
        } else if(Functions.sessionAttributes.STATE === Functions.states.RESULTMODE) {
            MESSAGE = Texts.FINAL_MESSAGE.speechOutput;
            REPROMP_MESSAGE = Texts.FINAL_MESSAGE.repromptSpeech;
        }
        return handlerInput.responseBuilder
            .speak(MESSAGE)
            .reprompt(REPROMP_MESSAGE)
            .getResponse();
    }
};
const FallbackIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && handlerInput.requestEnvelope.request.intent.name === 'AMAZON.FallbackIntent';
    },
    handle(handlerInput) {
        return handlerInput.responseBuilder
            .speak(Texts.MISUNDERSTOOD_INSTRUCTIONS_ANSWER)
            .getResponse();
    }
}
const HelpIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && handlerInput.requestEnvelope.request.intent.name === 'AMAZON.HelpIntent';
    },
    handle(handlerInput) {
        return handlerInput.responseBuilder
            .speak(Texts.HELP_MESSAGE_AFTER_START)
            .reprompt(Texts.HELP_MESSAGE_AFTER_START)
            .getResponse();
    }
};
const CancelAndStopIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && (handlerInput.requestEnvelope.request.intent.name === 'AMAZON.CancelIntent'
                || handlerInput.requestEnvelope.request.intent.name === 'AMAZON.StopIntent');
    },
    handle(handlerInput) {
        return handlerInput.responseBuilder
            .speak(Texts.CANCEL_MESSAGE)
            .withShouldEndSession(true)
            .getResponse();
    }
};
const SessionEndedRequestHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'SessionEndedRequest';
    },
    handle(handlerInput) {
        return handlerInput.responseBuilder
            .speak(Texts.GOODBYE_MESSAGE)
            .getResponse();
        // Any cleanup logic goes here.
        //return handlerInput.responseBuilder.getResponse();
    }
};

// The intent reflector is used for interaction model testing and debugging.
// It will simply repeat the intent the user said. You can create custom handlers
// for your intents by defining them above, then also adding them to the request
// handler chain below.
const IntentReflectorHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest';
    },
    handle(handlerInput) {
        const intentName = handlerInput.requestEnvelope.request.intent.name;
        const speechText = `You just triggered ${intentName}`;

        return handlerInput.responseBuilder
            .speak(speechText)
            //.reprompt('add a reprompt if you want to keep the session open for the user to respond')
            .getResponse();
    }
};

// Generic error handling to capture any syntax or routing errors. If you receive an error
// stating the request handler chain is not found, you have not implemented a handler for
// the intent being invoked or included it in the skill builder below.
const ErrorHandler = {
    canHandle() {
        return true;
    },
    handle(handlerInput, error) {
        console.log(`~~~~ Error handled: ${error.message}`);
        const speechText = `Sorry, I couldn't understand what you said. Please try again.`;

        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt(speechText)
            .getResponse();
    }
};


// This handler acts as the entry point for your skill, routing all request and response
// payloads to the handlers above. Make sure any new handlers or interceptors you've
// defined are included below. The order matters - they're processed top to bottom.
exports.handler = Alexa.SkillBuilders.custom()
    .addRequestHandlers(
        LaunchRequestHandler,
        YesIntentHandler, 
        NoIntentHandler, 
        ResultIntentHandler, 
        NextQuestionIntentHandler, 
        RepeatIntentHandler, 
        FallbackIntentHandler,
        HelpIntentHandler,
        CancelAndStopIntentHandler,
        SessionEndedRequestHandler,
        IntentReflectorHandler) // make sure IntentReflectorHandler is last so it doesn't override your custom intent handlers
    .addErrorHandlers(
        ErrorHandler)
    .lambda();
