const WAIT_TIME_TO_SHORT_ANSWER = 90; //Seconds
const ANSWERS = [];
const COMBINATIONS = {
  '11111':['dungbeetle'],
  '00000':['lion'],
  '10000':['commonnightingale','froghopper','starfish','bottlenosedolphin','lion'],
  '01000':['froghopper','starfish','bottlenosedolphin'],
  '00100':['commonnightingale','starfish','bottlenosedolphin'],
  '00010':['commonnightingale','froghopper','bottlenosedolphin'],
  '00001':['commonnightingale','froghopper','starfish'],
  '01111':['dungbeetle'],
  '10111':['pygmythreetoedsloth','maleprayingmantis','domesticturkey'],
  '11011':['africanwildass','maleprayingmantis','domesticturkey'],
  '11101':['africanwildass','pygmythreetoedsloth','domesticturkey'],
  '11110':['africanwildass','pygmythreetoedsloth','maleprayingmantis'],
  '11000':['froghopper','starfish','bottlenosedolphin'],
  '10100':['commonnightingale','starfish','bottlenosedolphin'],
  '10010':['commonnightingale','froghopper','bottlenosedolphin'],
  '10001':['commonnightingale','froghopper','starfish'],
  '01100':['starfish','bottlenosedolphin'],
  '01010':['froghopper','bottlenosedolphin'],
  '01001':['froghopper','starfish'],
  '00110':['commonnightingale','bottlenosedolphin'],
  '00101':['commonnightingale','starfish'],
  '00011':['commonnightingale','froghopper'],
  '11100':['africanwildass','pygmythreetoedsloth'],
  '11010':['africanwildass','maleprayingmantis'],
  '11001':['africanwildass','domesticturkey'],
  '10110':['pygmythreetoedsloth','maleprayingmantis'],
  '10011':['maleprayingmantis','domesticturkey'],
  '10101':['pygmythreetoedsloth','domesticturkey'],
  '01110':['africanwildass','pygmythreetoedsloth','maleprayingmantis'],
  '00111':['pygmythreetoedsloth','maleprayingmantis','domesticturkey'],
  '01101':['africanwildass','pygmythreetoedsloth','domesticturkey'],
  '01011':['africanwildass','maleprayingmantis','domesticturkey'],
}

const SKILL_NAME = "The Wheel of Animal Reincarnation";
const HELP_MESSAGE_BEFORE_START = "Answer five questions, and you'll hear your animal destiny. Ready to play?";
const HELP_MESSAGE_AFTER_START = "Just respond with yes or no and I'll predict your animal after-life.";
const HELP_REPROMPT = "Even a yep or a nope will do. Answer my questions you silly goose.";
const STOP_MESSAGE = "Alright, <prosody rate='fast'>see ya</prosody> <prosody volume='x-loud'>later</prosody> <prosody rate='fast'>alli</prosody> <prosody volume='x-loud'>gator!</prosody>";
const CANCEL_MESSAGE = "Come back when you're not chicken <break time='.3s'/> <amazon:effect name='whispered'>chicken.</amazon:effect> <audio src='soundbank://soundlibrary/animals/amzn_sfx_chicken_cluck_01'/>";
const GOODBYE_MESSAGE = "Bye-bye-birdie!";
const MISUNDERSTOOD_INSTRUCTIONS_ANSWER = "It's not rocket science, just give me a yes, yep, yeah, nah, nope, or no.";

const WELCOME_MESSAGE = "<prosody rate='fast'>Hello</prosody> <prosody rate='slow'>there</prosody> <prosody rate='fast'>time wasters</prosody> <break time='.3s'/> and welcome, to the wheel of <prosody volume='x-loud'>animal reincarnation.</prosody> The quiz game that will reveal your <emphasis level='reduced'>animal future!</emphasis> <audio src='soundbank://soundlibrary/human/amzn_sfx_crowd_applause_01'/> Answer five yes or no questions and we'll let you know where you'll end up in the after-life food chain. Ready to play?";
const SHORT_WELCOME_MESSAGE = "Welcome back to the wheel of animal reincarnation, ready to spin again?";
const REPEATED_WELCOME_MESSAGE = 'Ready to spin the wheel of animal reincarnation?';
const INITIAL_QUESTION_INTROS = [
  "Great! Let's start!",
  "<say-as interpret-as='interjection'>Alrighty</say-as>! Here comes your first question!",
  "Ok let's go.",
  "Alright, let's do this!"
];
const QUESTION_INTROS = [
  "Oh poop!",
  "Okey Dokey.",
  "Uh Oh!",
  "Interesting.",
  "You're just like your brother.",
  "Of course.",
  "You're a liar.",
  "Oh-no you didn't!.",
  "Right on!.",
  "That's, what-she-said!"
];
const UNDECISIVE_RESPONSES = [
  "<say-as interpret-as='interjection'>Honk</say-as>. I'll just choose for you.",
  "<say-as interpret-as='interjection'>Nanu Nanu</say-as>. I picked an answer for you.",
  "<say-as interpret-as='interjection'>Uh oh</say-as>... well nothing I can do about that.",
  "<say-as interpret-as='interjection'>Aha</say-as>. We will just move on then.",
  "<say-as interpret-as='interjection'>Aw man</say-as>. How about this question?",
];
const RESULT_MESSAGE = "Here comes the big reveal! In your next life <break time='.2s'/> <prosody rate='slow'>you will be</prosody> <break time='.5s'/>"; // the name of the result is inserted here.
const PLAY_AGAIN_REQUEST = "The wheel of animal reincarnation has come to rest. Do you want to try your luck again?";
const FINAL_MESSAGE = { 
  speechOutput: null, 
  repromptSpeech: null,
  cardTitle: null,
  cardContent: null,
  imageObj: null
};
const prependMessage = '';

const animalList = {
  commonnightingale: {
    name: "a common nightingale.",
    display_name: "Common Nightingale",
    audio_message: "Your days of singing only in the shower are over. You may be a common nightingale, but you are certainly anything but common. You're like the Usher of the bird world. You sing all night long during mating season to lure the night-in-gals for a little sumpin-sumpin. Now <say-as interpret-as='interjection'>go sing</say-as><say-as interpret-as='interjection'>your little birdy</say-as> heart out!<audio src='soundbank://soundlibrary/animals/amzn_sfx_bird_forest_01'/>",
    description: "The name nightingale means night songstress and the bird has become legendary for its beautiful singing voice that has inspired songs, fairy tales, operas, and even books.  ",
    img: {
      smallImageUrl: "https://image.ibb.co/gRTPYz/common_nightingale_small.jpg",
      largeImageUrl: "https://image.ibb.co/bV606K/common_nightingale_big.jpg"
    }
  },
  africanwildass: {
    name: "an african wild ass.",
    display_name: "African Wild Ass",
    audio_message: "Your singing days have come and gone Sinatra <break time='.3s'/> Just try charming someone with that hideous hee-haw of yours... ",
    description: "You're the ancestor of the donkey and even though you may be wild, you're still an ass. Take comfort in this proverb from Afghanistan: 'Even if a donkey goes to Mecca, he is still a donkey.' Or this proverb from Ehtiopia: 'The heifer that spends time with a donkey learns to fart.'",
    img: {
      smallImageUrl: "https://image.ibb.co/n3fhLe/african_wild_ass_small.jpg",
      largeImageUrl: "https://image.ibb.co/dHCwfe/african_wild_ass_big.jpg"
    }
  },
  froghopper: {
    name: "a froghopper.<audio src='soundbank://soundlibrary/cartoon/amzn_sfx_boing_short_1x_01'/>",
    display_name: "Froghopper",
    audio_message: "So athletic ability wasn't your strong suit? You are in luck! The froghopper is the undisputed high jump champion of the world, with the ability to jump seventy times its own body height. You may be a tiny green bug but you can jump real freaking high.<audio src='soundbank://soundlibrary/cartoon/amzn_sfx_boing_short_1x_01'/>",
    description: "Unfortunately your days of not dunking a basketball will continue into the next life as the froghopper stands just 6 milimeters tall. But I don't think that will stop you from impressing all the lady bugs out there, wink wink. ",
    img: {
      smallImageUrl: "https://image.ibb.co/cX9gDz/froghopper_small.jpg",
      largeImageUrl: "https://image.ibb.co/hcfz0e/froghopper_big.jpg"
    }
  },
  pygmythreetoedsloth: {
    name: "a pygmy, three-toed sloth.",
    display_name: "Pygmy Three-Toed Sloth",
    audio_message: "So you fancy yourself as some sort of athlete, huh? Well, not anymore, <say-as interpret-as='interjection'>slothy!</say-as> <break time='.3s'/> Slothily slow and steady ain't gonna win this race!  I mean <break time='.3s'/> you make molasses look like niagra falls!<audio src='soundbank://soundlibrary/musical/amzn_sfx_drum_comedy_01'/> Good luck trying to dunk with your toes.<audio src='soundbank://soundlibrary/cartoon/amzn_sfx_boing_med_1x_02'/>",
    description: "The pygmy three-toed sloth also known as the dwarf sloth is critically endangered almost certainly because at .15 miles per hour it is on of the slowest animals on the planet. They cannot walk on all four limbs, so they must use their front arms and claws to drag themselves across the rainforest floor. ",
    img: {
      smallImageUrl: "https://image.ibb.co/jBgiRK/pygmy_three_toed_sloth_small.jpg",
      largeImageUrl: "https://image.ibb.co/hXqA6K/pygmy_three_toed_sloth_bigger.jpg"
    }
  },
  starfish: {
    name: "a starfish.",
    display_name: "Starfish",
    audio_message: "I've got some good news and some bad news. I'll give you the bad news first. You're alone now... and you're going to be alone in the next life too.  The good news is that you know how to reproduce asexually. No significant other? No problem!",
    description: "When a starfish eats, its stomach exits its mouth to digest the food and returns when it has finished. You know what that means? No more brushing or flossing, and no more heartburn!",
    img: {
      smallImageUrl: "https://image.ibb.co/fQfXLe/starfish_small.jpg",
      largeImageUrl: "https://image.ibb.co/dg6iRK/starfish_big.jpg"
    }
  },
  maleprayingmantis: {
    name: "a male, praying mantis.",
    display_name: "Male Praying Mantis",
    audio_message: "<prosody rate='slow'>Well-well-well</prosody>. You have a significant other and you think you're cool, well I've got some bad news for you. The expression She bit my head off., is going to have a <prosody rate='x-slow'>whole new</prosody> meaning for you.",
    description: "Around ninety percent of mantises participate in sexual cannibalism. Some of the males escape but since multiple matings occur the probability of a male being eaten increases cumulatively. It's only a matter of time until your lady friend eats your head off. Enjoy it while it lasts.",
    img: {
      smallImageUrl: "https://image.ibb.co/heHORK/male_praying_mantis_small.jpg",
      largeImageUrl: "https://image.ibb.co/nwo8tz/male_praying_mantis_big.jpg"
    }
  },
  bottlenosedolphin: {
    name: "a bottlenose dolphin.",
    display_name: "Bottlenose Dolphin",
    audio_message: "Ok, so you weren't the sharpest tool in the shed, but the good news is that you've reincarnated into one of the most intelligent animals in the world. Dolphins are the only animal to have passed the mirror intelligence test, which means they can recognize themselves in a mirror. That will really come in handy for all those times when you need to recognize yourself in a mirror. Yay for you! Enjoy being smart... ",
    description: "Dolphins have further proved their intelligence by being one of the only animals to engage in recreational sex. As intelligent as they may be, they seem inexplicably prone to ending up in Seaworld...you've been warned.",
    img: {
      smallImageUrl: "https://image.ibb.co/euKnmK/bottlenose_dolphin_small.jpg",
      largeImageUrl: "https://image.ibb.co/mwSU0e/bottlenose_dolphin_big.jpg"
    }
  },
  domesticturkey: {
    name: "a domestic turkey.",
    display_name: "Domestic Turkey",
    audio_message: "<audio src='soundbank://soundlibrary/animals/amzn_sfx_turkey_gobbling_01'/> Ok Mr. Math-smarty pants, you've had your day in the sun, now it's time to gobble, gobble-gobble. You cannot fly, and will probably get eaten soon because you are as delicious, as you are ugly. Good luck with that...",
    description: "You will find the turkey on nearly every dumb animal list on the planet probably due to the fact that they have tetanic torticollar spasms which cause them to stare into the sky for long periods of time, even in the rain. Male turkeys are so dumb that they have evolved a fleshy protuberance that hangs from the top of their beaks to store just a little bit of extra stupidity. It's called a snood.  Isn't that dumb?",
    img: {
      smallImageUrl: "https://image.ibb.co/d3mBDz/domestic_turkey_small.jpg",
      largeImageUrl: "https://image.ibb.co/dtjL6K/domestic_turkey_big.jpg"
    }
  },
  lion: {
    name: "a lion.",
    display_name: "Lion",
    audio_message: "Ok... so you are telling me that you can't sing, have no athletic ability, are alone, and aren't smart either? We feel super sorry for you. It's time you had a change of luck. Welcome to the top of the food chain, king of the jungle. <audio src='soundbank://soundlibrary/animals/amzn_sfx_lion_roar_02'/>",
    description: "Nobody messes with a lion...except of course for poachers. So...watchout for poachers!",
    img: {
      smallImageUrl: "https://image.ibb.co/bKvA6K/lion_small.jpg",
      largeImageUrl: "https://image.ibb.co/dcr6fe/lion_big.jpg"
    }
  },
dungbeetle: {
    name: "a dung beetle.",
    display_name: "Dung Beetle",
    audio_message: "Alright superstar you have had quite enough attention already. No one person should have it all. It's time for you to eat <say-as interpret-as='expletive'>Shit</say-as> <break time='.5s'/> <prosody rate='slow'>I mean</prosody> <break time='.2s'/> dung <break time='.5s'/> <prosody volume='x-loud'>a crap load</prosody> of dung.<audio src='soundbank://soundlibrary/home/amzn_sfx_toilet_flush_02'/>",
    description: "Some dung beetles are called rollers because they roll up the dung into balls, others are called tunnelers because they bury it and save it for later, still others are called dwellers because they dwell all up in that dung. We'll leave the decision to you.",
    img: {
      smallImageUrl: "https://image.ibb.co/bNm06K/dung_beetle_small.jpg",
      largeImageUrl: "https://image.ibb.co/fW5otz/dung_beetle_big.jpg"
    }
  }
};

const questions = [
  {
    question: "Have you ever owned a bird?"
  },
  {
    question: "Can you sing in tune?"
  },
  {
    question: "Can you dunk a basketball?"
  },
  {
    question: "Do you have a significant other?"
  },
  {
    question: "Are you good at math?"
  }
];