// Private methods (this is the actual code logic behind the app)

//const _initializeApp = handler => {
  // Set the progress to -1 one in the beginning
//  handler.attributes['questionProgress'] = -1;
//};

var sessionAttributes = {TIMESTAMP_WELCOME_MESSAGE:0,QUESTION_PROGRESS:-1,STATE:'_INITIAL'};

function supportsAPL(handlerInput) {
	const supportedInterfaces = handlerInput.requestEnvelope.context.System.device.supportedInterfaces;
	const aplInterface = supportedInterfaces['Alexa.Presentation.APL'];
	return aplInterface !== null && aplInterface !== undefined;
}

const _nextQuestionOrResult = (handlerinput,prependMessage = '') => {
    if(sessionAttributes.QUESTION_PROGRESS >= (questions.length - 1)){
      sessionAttributes.STATE = states.RESULTMODE;
      return ResultIntentHandler.handle(handlerinput);
    }else{
      return NextQuestionIntentHandler.handle(handlerinput);
    }
  };
  
const _recordanswer = (handler, answer) => {
    ANSWERS.push(answer);
    if(ANSWERS.length > 5) {
        ANSWERS.shift();
    }
};
  
const _randomQuestionIntro = handler => {
    if(sessionAttributes.QUESTION_PROGRESS === 0){
      // return random initial question intro if it's the first question:
      return _randomOfArray(INITIAL_QUESTION_INTROS);
    }else{
      // Assign all question intros to remainingQuestionIntros on the first execution:
      var remainingQuestionIntros = remainingQuestionIntros || QUESTION_INTROS;
      // randomQuestion will return 0 if the remainingQuestionIntros are empty:
      let randomQuestion = remainingQuestionIntros.splice(_randomIndexOfArray(remainingQuestionIntros), 1);
      // Remove random Question from rameining question intros and return the removed question. If the remainingQuestions are empty return the first question:
      return randomQuestion ? randomQuestion : QUESTION_INTROS[0];
    }
};
  
const _randomIndexOfArray = (array) => Math.floor(Math.random() * array.length);
const _randomOfArray = (array) => array[_randomIndexOfArray(array)];
  
const states = {
    INITIALMODE: "_INITIAL",
    QUIZMODE: "_QUIZMODE",
    RESULTMODE: "_RESULTMODE"
};